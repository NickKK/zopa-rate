package org.zopa.zoparate.filereaders;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.core.io.ClassPathResource;
import org.zopa.zoparate.model.Offer;
import java.util.List;
import java.util.stream.Stream;

class MarketCsvReaderTest {

    @ParameterizedTest
    @MethodSource("createInvalidInputs")
    void readShouldThrowAFileNotFoundExceptionWhenTheCSVFileIsNotProvided(String marketCsvFilePath, String[] headers) {
        // Given
        MarketCsvReader marketCsvReader = new MarketCsvReader(new ClassPathResource(marketCsvFilePath), headers);

        // Expect
        Throwable throwable = Assertions.assertThrows(RuntimeException.class, marketCsvReader::read);
        Assertions.assertEquals("Please a provide a market csv file, place your file under the `resources/temp` directory",
                                throwable.getMessage());
    }

    private static Stream<Arguments> createInvalidInputs() {
        return Stream.of(Arguments.of("mock", new String[] {"Lender", "Rate" ,"Available"}));
    }

    @ParameterizedTest
    @MethodSource("createValidInputs")
    void readShouldReadSuccessfullyTheMarketOffersFromTheCSVFile(String marketCsvFilePath, String[] headers) {
        // Given
        MarketCsvReader marketCsvReader = new MarketCsvReader(new ClassPathResource(marketCsvFilePath), headers);

        // When
        List<Offer> offers = marketCsvReader.read();

        // Then
        Assertions.assertEquals(7, offers.size());
    }

    private static Stream<Arguments> createValidInputs() {
        return Stream.of(Arguments.of("temp/MarketDataforExercise-csv.csv", new String[] {"Lender", "Rate" ,"Available"}));
    }
}