package org.zopa.zoparate.service;

import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.zopa.zoparate.filereaders.MarketCsvReader;
import org.zopa.zoparate.model.Offer;
import org.zopa.zoparate.model.QuoteRequestDto;
import org.zopa.zoparate.model.QuoteResponseDto;
import org.zopa.zoparate.service.calculators.InterestRateCalculatorService;
import org.zopa.zoparate.service.calculators.RepaymentCalculatorService;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.*;

class QuoteServiceTest {
    private final MarketCsvReader marketCsvReaderMock = mock(MarketCsvReader.class);
    private final InterestRateCalculatorService interestRateCalculatorServiceMock = mock(InterestRateCalculatorService.class);
    private final RepaymentCalculatorService repaymentCalculatorServiceMock = mock(RepaymentCalculatorService.class);
    private final QuoteService quoteService = new QuoteService(marketCsvReaderMock,
                                                               interestRateCalculatorServiceMock,
                                                               repaymentCalculatorServiceMock,
                                                               "1000",
                                                               "15000",
                                                               "100");

    @ParameterizedTest
    @ValueSource(ints = { 900, 20000, 1050})
    void generateShouldThrowAnIllegalArgumentExceptionWhenRequestPayloadIsNotValid(int requestedLoanAmount) {
        // Given
        QuoteRequestDto quoteRequestDto = QuoteRequestDto.builder()
                                                         .requestedAmount(BigDecimal.valueOf(requestedLoanAmount))
                                                         .termInMonths(36)
                                                         .build();
        // Expect
        Assertions.assertThrows(IllegalArgumentException.class, () -> quoteService.generate(quoteRequestDto));
    }

    @Test
    void generateShouldReturnTheProperQuote() {
        // Given
        QuoteRequestDto quoteRequestDto = QuoteRequestDto.builder()
                                                         .requestedAmount(BigDecimal.valueOf(1000))
                                                         .termInMonths(36)
                                                         .build();
        Offer offer1 = Offer.builder().availableAmount(BigDecimal.valueOf(480)).interestRate(BigDecimal.valueOf(0.069)).build();
        Offer offer2 = Offer.builder().availableAmount(BigDecimal.valueOf(520)).interestRate(BigDecimal.valueOf(0.071)).build();
        List<Offer> offersStub = Lists.newArrayList(offer1, offer2);
        BigDecimal lowestRateStub = BigDecimal.valueOf(0.07);
        BigDecimal monthlyInterestRateStub = BigDecimal.valueOf(0.0056);
        BigDecimal monthlyRepaymentStub = BigDecimal.valueOf(30.78);
        BigDecimal totalRepaymentStub = BigDecimal.valueOf(1108.04);

        when(marketCsvReaderMock.read()).thenReturn(offersStub);
        when(interestRateCalculatorServiceMock.provideLowestRate(quoteRequestDto.getRequestedAmount(), offersStub)).thenReturn(lowestRateStub);
        when(interestRateCalculatorServiceMock.calculateMonthlyInterestRate(lowestRateStub)).thenReturn(monthlyInterestRateStub);
        when(repaymentCalculatorServiceMock.calculateMonthlyRepayment(quoteRequestDto.getRequestedAmount(), monthlyInterestRateStub, quoteRequestDto.getTermInMonths()))
                .thenReturn(monthlyRepaymentStub);
        when(repaymentCalculatorServiceMock.calculateTotalRepayment(monthlyRepaymentStub, quoteRequestDto.getTermInMonths())).thenReturn(totalRepaymentStub);

        // When
        QuoteResponseDto actualQuoteResponseDto = quoteService.generate(quoteRequestDto);

        // Then
        // Verify that mocks invoked the wright number of times with the wright args
        verify(marketCsvReaderMock, times(1)).read();
        verify(interestRateCalculatorServiceMock, times(1))
                .provideLowestRate(quoteRequestDto.getRequestedAmount(), offersStub);
        verify(interestRateCalculatorServiceMock, times(1)).calculateMonthlyInterestRate(lowestRateStub);
        verify(repaymentCalculatorServiceMock, times(1))
                .calculateMonthlyRepayment(quoteRequestDto.getRequestedAmount(), monthlyInterestRateStub, quoteRequestDto.getTermInMonths());
        verify(repaymentCalculatorServiceMock, times(1))
                .calculateTotalRepayment(monthlyRepaymentStub, quoteRequestDto.getTermInMonths());

        QuoteResponseDto expectedQuoteResponseDto = QuoteResponseDto.builder()
                                                                    .requestedAmount("£1,000.00")
                                                                    .annualInterestRate("7.0%")
                                                                    .monthlyRepayment("£30.78")
                                                                    .totalRepayment("£1,108.04")
                                                                    .build();
        Assert.assertEquals(expectedQuoteResponseDto, actualQuoteResponseDto);

    }
}