package org.zopa.zoparate.service.calculators;

import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.zopa.zoparate.model.Offer;

import java.math.BigDecimal;
import java.util.List;

class InterestRateCalculatorServiceTest {
    private final InterestRateCalculatorService interestRateCalculatorService = new InterestRateCalculatorService();

    @Test
    void provideLowestRateShouldReturnTheLowestRate() {
        // Given
        BigDecimal loanAmount = BigDecimal.valueOf(1000);
        Offer offer1 = Offer.builder()
                            .availableAmount(BigDecimal.valueOf(480))
                            .interestRate(BigDecimal.valueOf(0.069))
                            .build();
        Offer offer2 = Offer.builder()
                            .availableAmount(BigDecimal.valueOf(520))
                            .interestRate(BigDecimal.valueOf(0.071))
                            .build();
        List<Offer > offers = Lists.newArrayList(offer1, offer2);

                // When
        BigDecimal lowestAnnualInterestRate = interestRateCalculatorService.provideLowestRate(loanAmount, offers);

        // Then
        BigDecimal expectedResult = BigDecimal.valueOf(0.07);
        Assert.assertTrue(expectedResult.compareTo(lowestAnnualInterestRate) == 0);
    }

    @Test
    void provideLowestRateShouldThrowAnExceptionWhenOffersAreNotEnoughToFulfilTheAmount() {
        // Given
        BigDecimal loanAmount = BigDecimal.valueOf(2400);
        Offer offer1 = Offer.builder()
                .availableAmount(BigDecimal.valueOf(480))
                .interestRate(BigDecimal.valueOf(0.069))
                .build();
        Offer offer2 = Offer.builder()
                .availableAmount(BigDecimal.valueOf(520))
                .interestRate(BigDecimal.valueOf(0.071))
                .build();
        List<Offer > offers = Lists.newArrayList(offer1, offer2);

        // Expect
        Throwable throwable= Assertions.assertThrows(IllegalArgumentException.class,
                () -> interestRateCalculatorService.provideLowestRate(loanAmount, offers));

        Assertions.assertEquals("Not enough offers in order to fulfil the requested loan amount",
                throwable.getMessage());
    }

    @Test
    void calculateMonthlyInterestRateShouldReturnTheProperTheMonthlyInterestRate() {
        // Given
        BigDecimal annualInterestRate = BigDecimal.valueOf(0.07);

        // When
        BigDecimal actualMonthlyInterestRate = interestRateCalculatorService.calculateMonthlyInterestRate(annualInterestRate);

        // Then
        BigDecimal expectedMonthlyInterestRate = BigDecimal.valueOf(0.0056541453874053);
        Assert.assertEquals(expectedMonthlyInterestRate, actualMonthlyInterestRate);
    }
}