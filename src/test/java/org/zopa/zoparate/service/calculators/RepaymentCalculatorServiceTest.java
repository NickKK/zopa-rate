package org.zopa.zoparate.service.calculators;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.zopa.zoparate.utils.AmortizationFormula;
import java.math.BigDecimal;
import static org.mockito.Mockito.*;

class RepaymentCalculatorServiceTest {
    private AmortizationFormula amortizationFormulaMock = mock(AmortizationFormula.class);
    private RepaymentCalculatorService repaymentCalculatorService = new RepaymentCalculatorService(amortizationFormulaMock);

    @Test
    void calculateTotalRepaymentShouldReturnTheProperTotalRepayment() {
        // Given
        BigDecimal monthlyRepayment = BigDecimal.valueOf(1);
        Integer termInMonths = 36;

        // When
        BigDecimal actualTotalRepayment = repaymentCalculatorService.calculateTotalRepayment(monthlyRepayment, termInMonths);

        // Then
        BigDecimal expectedTotalRepayment = BigDecimal.valueOf(36);
        Assert.assertEquals(expectedTotalRepayment, actualTotalRepayment);
    }

    @Test
    void calculateMonthlyRepaymentShouldReturnTheProperMonthlyRepayment() {
        // Given
        BigDecimal loanAmount = BigDecimal.valueOf(1000);
        BigDecimal monthlyInterestRate = BigDecimal.valueOf(0.0056);
        Integer termInMonths = 36;
        when(amortizationFormulaMock.apply(loanAmount, monthlyInterestRate, termInMonths)).thenReturn(BigDecimal.valueOf(30.78));

        // When
        BigDecimal actualMonthlyPayment = this.repaymentCalculatorService.calculateMonthlyRepayment(loanAmount, monthlyInterestRate, termInMonths);

        // Then
        // Verify that mocks invoked the wright number of times with the wright args
        verify(amortizationFormulaMock, times(1)).apply(loanAmount, monthlyInterestRate, termInMonths);
        BigDecimal expectedMonthlyPayment = BigDecimal.valueOf(30.78);
        Assert.assertEquals(expectedMonthlyPayment, actualMonthlyPayment);
    }
}