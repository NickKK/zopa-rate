package org.zopa.zoparate.utils;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

class AmortizationFormulaTest {
    private final AmortizationFormula amortizationFormula = new AmortizationFormula();


    @Test
    void applyShouldReturnTheProperPeriodicPaymentWhenInterestIsZero() {
        // Given
        BigDecimal amount = BigDecimal.valueOf(36);
        BigDecimal periodicInterestRate = BigDecimal.ZERO;
        Integer totalNumberOfPayments = 36;

        // When
        BigDecimal actualPeriodicPaymentAmount = amortizationFormula.apply(amount, periodicInterestRate, totalNumberOfPayments);

        // Then
        BigDecimal expectedPeriodicPaymentAmount = BigDecimal.ONE;
        Assert.assertEquals(expectedPeriodicPaymentAmount, actualPeriodicPaymentAmount);
    }

    @Test
    void applyShouldReturnTheProperPeriodicPaymentWhenInterestIsGreaterThanZero() {
        // Given
        BigDecimal amount = BigDecimal.valueOf(1000);
        BigDecimal periodicInterestRate = BigDecimal.valueOf(0.0056);
        Integer totalNumberOfPayments = 36;

        // When
        BigDecimal actualPeriodicPaymentAmount = amortizationFormula.apply(amount, periodicInterestRate, totalNumberOfPayments);

        // Then
        BigDecimal expectedPeriodicPaymentAmount = BigDecimal.valueOf(30.7492);
        Assert.assertEquals(expectedPeriodicPaymentAmount, actualPeriodicPaymentAmount);
    }

}