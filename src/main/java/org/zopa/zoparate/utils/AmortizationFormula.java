package org.zopa.zoparate.utils;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * The Amortization Formula
 *
 * A = (P * i) / 1 - (1 + i)^-n
 *
 * A = periodic payment amount
 * P = amount of principal, net of initial payments
 * i = periodic interest rate
 * n = total number of payments
 */
@Component
public class AmortizationFormula {

    /**
     * Applies the amortization formula
     *
     * @param amount The loan amount
     * @param periodicInterestRate The periodic interest rate
     * @param totalNumberOfPayments The total number of payments
     * @return The periodic payment amount
     */
    public BigDecimal apply(BigDecimal amount, BigDecimal periodicInterestRate, Integer totalNumberOfPayments) {
        if (periodicInterestRate.compareTo(BigDecimal.ZERO) == 0)
            return amount.divide(BigDecimal.valueOf(totalNumberOfPayments), RoundingMode.HALF_EVEN);

        BigDecimal numerator = amount.multiply(periodicInterestRate);
        double power = Math.pow(BigDecimal.ONE.add(periodicInterestRate).doubleValue(), -totalNumberOfPayments);
        BigDecimal divisor = BigDecimal.ONE.subtract(BigDecimal.valueOf(power));

        return numerator.divide(divisor, RoundingMode.HALF_EVEN);
    }
}