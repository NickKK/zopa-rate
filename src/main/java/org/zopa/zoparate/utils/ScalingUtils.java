package org.zopa.zoparate.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Scaling Utilities
 */
public class ScalingUtils {

    public static BigDecimal scaleInterestRate(BigDecimal interestRate) {
        return interestRate.setScale(3, RoundingMode.HALF_EVEN).stripTrailingZeros();
    }

    public static BigDecimal scaleRepayment(BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_EVEN).stripTrailingZeros();
    }
}
