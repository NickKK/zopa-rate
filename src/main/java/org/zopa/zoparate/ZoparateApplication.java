package org.zopa.zoparate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZoparateApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZoparateApplication.class, args);
    }

}
