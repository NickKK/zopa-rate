package org.zopa.zoparate.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.FileNotFoundException;

@ControllerAdvice
public class ZopaRateExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = RuntimeException.class)
    protected ResponseEntity handleRuntimeException(RuntimeException runtimeException) {
        if (runtimeException.getCause() instanceof FileNotFoundException) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(runtimeException.getMessage());
        } else if (runtimeException instanceof IllegalArgumentException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(runtimeException.getMessage());
        } else {
            throw runtimeException;
        }
    }
}
