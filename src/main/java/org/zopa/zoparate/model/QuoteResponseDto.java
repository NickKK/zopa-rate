package org.zopa.zoparate.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The quote response dto
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuoteResponseDto {
    private String requestedAmount;
    private String annualInterestRate;
    private String monthlyRepayment;
    private String totalRepayment;
}
