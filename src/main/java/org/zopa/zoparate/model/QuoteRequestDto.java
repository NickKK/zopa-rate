package org.zopa.zoparate.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

/**
 * The quote request dto
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuoteRequestDto {
    private BigDecimal requestedAmount;
    private Integer termInMonths;
}
