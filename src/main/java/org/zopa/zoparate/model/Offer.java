package org.zopa.zoparate.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * The POJO that represents a lenders offer
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Offer implements Comparable<Offer>{
    private String lendersName;
    private BigDecimal availableAmount;
    private BigDecimal interestRate;

    @Override
    public int compareTo(Offer offer) {
        return this.interestRate.compareTo(offer.interestRate);
    }
}
