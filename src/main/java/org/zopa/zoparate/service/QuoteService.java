package org.zopa.zoparate.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.zopa.zoparate.model.Offer;
import org.zopa.zoparate.model.QuoteRequestDto;
import org.zopa.zoparate.model.QuoteResponseDto;
import org.zopa.zoparate.filereaders.MarketCsvReader;
import org.zopa.zoparate.service.calculators.InterestRateCalculatorService;
import org.zopa.zoparate.service.calculators.RepaymentCalculatorService;
import org.zopa.zoparate.utils.ScalingUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Service for generate a quote from Zopa’s market of lenders
 */
@Service
public class QuoteService {

    private final MarketCsvReader marketCsvReader;
    private final InterestRateCalculatorService interestRateCalculatorService;
    private final RepaymentCalculatorService repaymentCalculatorService;
    private final String minLoanAmountLimit;
    private final String maxLoanAmountLimit;
    private final String loanAmountStep;

    public QuoteService(MarketCsvReader marketCsvReader,
                        InterestRateCalculatorService interestRateCalculatorService,
                        RepaymentCalculatorService repaymentCalculatorService,
                        @Value("${loanAmount.minLimit}") String minLoanAmountLimit,
                        @Value("${loanAmount.maxLimit}") String maxLoanAmountLimit,
                        @Value("${loanAmount.step}") String loanAmountStep) {
        this.marketCsvReader = marketCsvReader;
        this.interestRateCalculatorService = interestRateCalculatorService;
        this.repaymentCalculatorService = repaymentCalculatorService;
        this.minLoanAmountLimit = minLoanAmountLimit;
        this.maxLoanAmountLimit = maxLoanAmountLimit;
        this.loanAmountStep = loanAmountStep;
    }

    /**
     * Generates a quote
     */
    public QuoteResponseDto generate(QuoteRequestDto quoteRequestDto) {
        validateRequestPayload(quoteRequestDto);
        BigDecimal requestedLoanAmount = quoteRequestDto.getRequestedAmount();
        Integer termInMonths = quoteRequestDto.getTermInMonths();

        List<Offer> offers = marketCsvReader.read();
        BigDecimal lowestAnnualInterestRate = interestRateCalculatorService.provideLowestRate(requestedLoanAmount, offers);
        BigDecimal monthlyInterestRate = this.interestRateCalculatorService.calculateMonthlyInterestRate(lowestAnnualInterestRate);
        BigDecimal monthlyRepayment = this.repaymentCalculatorService.calculateMonthlyRepayment(requestedLoanAmount, monthlyInterestRate, termInMonths);
        BigDecimal totalRepayment = this.repaymentCalculatorService.calculateTotalRepayment(monthlyRepayment, termInMonths);

        return responseBuilder(requestedLoanAmount, lowestAnnualInterestRate, monthlyRepayment, totalRepayment);
    }

    /**
     * Validates the request payload
     *
     * @param quoteRequestDto The request dto
     */
    private void validateRequestPayload(QuoteRequestDto quoteRequestDto) {
        BigDecimal minLimit = new BigDecimal(this.minLoanAmountLimit);
        BigDecimal maxLimit = new BigDecimal(this.maxLoanAmountLimit);
        BigDecimal step = new BigDecimal(loanAmountStep);
        BigDecimal loanAmount = quoteRequestDto.getRequestedAmount();

        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.UK);
        Assert.isTrue(loanAmount.compareTo(minLimit) >= 0,
                "Requested loan amount should be over " + currencyFormat.format(minLimit));
        Assert.isTrue(loanAmount.compareTo(maxLimit) <= 0,
                "Requested loan amount should be less than " + currencyFormat.format(maxLimit));
        Assert.isTrue(loanAmount.remainder(step).compareTo(BigDecimal.ZERO) == 0,
                "Requested loan amount should have a " + currencyFormat.format(step) + " step");
    }

    /**
     * Builds the quote response in the proper format
     */
    private QuoteResponseDto responseBuilder(BigDecimal requestedLoanAmount,
                                             BigDecimal annualInterestRate,
                                             BigDecimal monthlyRepayment,
                                             BigDecimal totalRepayment) {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.UK);
        String formattedRequestedLoanAmount = currencyFormat.format(requestedLoanAmount.doubleValue());
        String formattedMonthlyRepayment =  currencyFormat.format(ScalingUtils.scaleRepayment(monthlyRepayment));
        String formattedTotalRepayment = currencyFormat.format(ScalingUtils.scaleRepayment(totalRepayment));

        NumberFormat percentFormat = NumberFormat.getPercentInstance(Locale.UK);
        percentFormat.setMinimumFractionDigits(1);
        String formattedAnnualInterestRate = percentFormat.format(ScalingUtils.scaleInterestRate(annualInterestRate));

        return QuoteResponseDto.builder()
                .requestedAmount(formattedRequestedLoanAmount)
                .annualInterestRate(formattedAnnualInterestRate)
                .monthlyRepayment(formattedMonthlyRepayment)
                .totalRepayment(formattedTotalRepayment)
                .build();
    }
}
