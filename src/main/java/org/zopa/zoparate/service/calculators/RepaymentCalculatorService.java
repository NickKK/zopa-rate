package org.zopa.zoparate.service.calculators;

import org.springframework.stereotype.Service;
import org.zopa.zoparate.utils.AmortizationFormula;

import java.math.BigDecimal;

/**
 * Service for the repayment calculations
 */
@Service
public class RepaymentCalculatorService {
    private final AmortizationFormula amortizationFormula;

    public RepaymentCalculatorService(AmortizationFormula amortizationFormula) {
        this.amortizationFormula = amortizationFormula;
    }


    /**
     * Calculates the total repayment
     *
     * @param monthlyRepayment The monthly repayment
     * @param termInMonths The term in months
     * @return The total repayment
     */
    public BigDecimal calculateTotalRepayment(BigDecimal monthlyRepayment, Integer termInMonths) {
        return monthlyRepayment.multiply(BigDecimal.valueOf(termInMonths));
    }

    /**
     * Calculates the monthly repayment
     *
     * @param loanAmount The loan amount
     * @param monthlyInterestRate The monthly interest rate
     * @param termInMonths The loan term in months
     * @return The monthly repayment in {@link BigDecimal}
     */
    public BigDecimal calculateMonthlyRepayment(BigDecimal loanAmount, BigDecimal monthlyInterestRate, Integer termInMonths ) {
        return amortizationFormula.apply(loanAmount, monthlyInterestRate, termInMonths);
    }
}
