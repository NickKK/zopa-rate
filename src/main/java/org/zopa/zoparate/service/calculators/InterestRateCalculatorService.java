package org.zopa.zoparate.service.calculators;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.zopa.zoparate.model.Offer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import static java.util.stream.Collectors.toList;

/**
 * Service for the interest rate calculations
 */
@Service
public class InterestRateCalculatorService {

    /**
     * Given all the available offers, provides us with as low rate as is possible
     *
     * @return The lowest annual interest rate
     */
    public BigDecimal provideLowestRate(BigDecimal loanAmount, List<Offer> offers) {
        // Sorts the offers by rate - ascending
        final List<Offer> sortedOffersByRate = offers.stream().sorted().collect(toList());

        final List<Offer> lowRateOffers = new ArrayList<>();
        BigDecimal sum = BigDecimal.ZERO;
        boolean fulfilAmount = false;
        // Find the offers we need in order to fulfill the requested loan amount
        for (Offer offer : sortedOffersByRate) {
            if (fulfilAmount) break;

            sum = sum.add(offer.getAvailableAmount());
            if (sum.compareTo(loanAmount) > 0) {
                BigDecimal exceedAmount = sum.subtract(loanAmount);
                // The partly amount we need to lend from the specific lender
                BigDecimal partlyLendingAmount = offer.getAvailableAmount().subtract(exceedAmount);
                lowRateOffers.add(Offer.builder()
                                        .availableAmount(partlyLendingAmount)
                                        .interestRate(offer.getInterestRate())
                                        .build());
                fulfilAmount = true;
            } else if (sum.compareTo(loanAmount) < 0) {
                lowRateOffers.add(Offer.builder()
                                        .availableAmount(offer.getAvailableAmount())
                                        .interestRate(offer.getInterestRate())
                                        .build());
            } else {
                lowRateOffers.add(Offer.builder()
                                        .availableAmount(offer.getAvailableAmount())
                                        .interestRate(offer.getInterestRate())
                                        .build());
                fulfilAmount = true;
            }
        }
        Assert.isTrue(fulfilAmount, "Not enough offers in order to fulfil the requested loan amount");
        return this.calculateAverageInterestRate(loanAmount, lowRateOffers);
    }

    /**
     *  Calculates the weighted average of the annual interest rates
     */
    private BigDecimal calculateAverageInterestRate(BigDecimal loanAmount, List<Offer> offers) {
        return offers.stream()
                .map(offer -> offer.getAvailableAmount().multiply(offer.getInterestRate()))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO)
                .divide(loanAmount, RoundingMode.HALF_EVEN);
    }

    /**
     *  Given an annual interest rate, calculates the monthly equivalent
     *
     *  "THE FORMULA"
     *  monthlyInterestRate = (1 + r)^t/n – 1`
     *  r = The interest rate over time t.
     *  n = The number of intervals within t, for which interest is to be calculated
     *  monthlyInterestRate = The interest rate over time t/n.
     *
     * @param annualInterestRate The annual interest rate
     * @return The monthly interest rate
     */
    public BigDecimal calculateMonthlyInterestRate(BigDecimal annualInterestRate) {
        double power = Math.pow(BigDecimal.ONE.add(annualInterestRate).doubleValue(), 1.0/12);
        return BigDecimal.valueOf(power).subtract(BigDecimal.ONE);
    }
}