package org.zopa.zoparate.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zopa.zoparate.model.QuoteRequestDto;
import org.zopa.zoparate.model.QuoteResponseDto;
import org.zopa.zoparate.service.QuoteService;

@RestController
@RequestMapping(path = "/zopa-rate",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
public class QuoteController {

    private final QuoteService quoteService;

    public QuoteController(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    /**
     * Generates the quote
     */
    @PostMapping(path = "/quote")
    public ResponseEntity<QuoteResponseDto> generateQuote(@RequestBody QuoteRequestDto quoteRequestDto) {
        QuoteResponseDto quoteResponseDto = quoteService.generate(quoteRequestDto);
        return ResponseEntity.ok().body(quoteResponseDto);
    }
}
