package org.zopa.zoparate.filereaders;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.zopa.zoparate.model.Offer;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Market Csv Reader
 */
@Component
public class MarketCsvReader {
    private final Resource resource;
    private final String[] marketCsvHeaders;

    public MarketCsvReader(@Value("classpath:${market.csv.file}") Resource resource,
                           @Value("${market.csv.headers") String[] marketCsvHeaders) {
        this.resource = resource;
        this.marketCsvHeaders = marketCsvHeaders;
    }

    /**
     * Reads the market csv file
     *
     * @return {@link List<Offer>} A list with the market offers
     */
    public List<Offer> read() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(resource.getFile())))) {
            CSVParser records = CSVFormat.DEFAULT.withHeader(marketCsvHeaders).withFirstRecordAsHeader().parse(br);
            return records.getRecords()
                           .stream()
                           .map(csvRecordToOfferConverter())
                           .collect(Collectors.toList());
        } catch (IOException iOException) {
            if (iOException instanceof FileNotFoundException) {
                throw new RuntimeException("Please a provide a market csv file, place your file under the `resources/temp` directory", iOException);
            }
            throw new RuntimeException(iOException);
        }
    }

    /**
     * Returns a Function that converts a {@link CSVRecord} to an {@link Offer}
     */
    private Function<CSVRecord, Offer> csvRecordToOfferConverter() {
        return record -> Offer.builder()
                              .interestRate(new BigDecimal(record.get("Rate")))
                              .availableAmount(new BigDecimal(record.get("Available")))
                              .build();
    }
}
