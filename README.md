###  Zopa Technical Test

##### Technologies used 

- Java 8, Spring Boot, Maven, JUnit

##### Run The application 

1. Place your market csv file under the following directory `zoparate\src\main\resources\temp`

2. Run the Spring-Boot application

3. Make an http POST request at http://localhost:8080/zopa-rate/quote 
   with body `{"requestedAmount": 1000, "termInMonths": 36}` and content type `application/json`
   
   
###### *Notes:
* I made the assumption that no service fee applied to each loan contract         